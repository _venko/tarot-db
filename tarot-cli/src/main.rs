#[macro_use]
extern crate serde_derive;

mod card;
mod edit;
mod file;
mod deck;

use card::Card;
use file::read_file;
use std::error::Error;

fn main() {
    let hermit_file = "/home/venko/code/tarot-db/decks/rws/major/09-hermit.yaml".to_owned();
    match read_file(hermit_file) {
        Err(why) => panic!("{}", why.description()),
        Ok(text) => {
            println!("{}", text);
            match Card::from_yaml(text) {
                Err(why) => panic!("{}", why.description()),
                Ok(card) => {
                    println!("{:?}", card);
                }
            }
        }
    };
}

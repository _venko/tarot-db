use std::fs::File;
use std::io;
use std::io::prelude::Read;

/// Reads the contents of a file and returns them as a String.
pub fn read_file(file_path: String) -> io::Result<String> {
    let mut file = File::open(file_path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    return Ok(contents);
}

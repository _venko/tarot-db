use crate::card::{Arcana, Card, Suit};

pub type Title = String;
pub type Keyword = String;
pub type Index = usize;

/// A trait for applying an edit to a card.
trait Apply {
    fn apply(self: &Self, card: &mut Card);
}

/// A trait for reversing the changes made by an edit.
trait Undo {
    fn undo(self: &Self) -> Self;
}

/// Represents an edit to the keyword set of a card.
#[derive(PartialEq, Eq, Clone, Debug)]
pub enum KeywordEdit {
    /// Represents inserting a keyword into the keywords vector at a given
    /// index (before the keyword already present at the given index).
    Insert(Keyword, Index),
    /// Represents deleting the keyword at a given index. The keyword part
    /// of this type is required for undoing a deletion.
    Delete(Keyword, Index),
    /// Represents swapping the keywords at two given indices.
    Swap(Index, Index),
}

impl Apply for KeywordEdit {
    fn apply(&self, card: &mut Card) {
        match self {
            KeywordEdit::Insert(word, idx) => {
                card.keywords.insert(*idx, word.to_string());
            }
            KeywordEdit::Delete(_, idx) => {
                card.keywords.remove(*idx);
            }
            KeywordEdit::Swap(idx1, idx2) => {
                card.keywords.swap(*idx1, *idx2);
            }
        }
    }
}

impl Undo for KeywordEdit {
    fn undo(&self) -> Self {
        match self {
            KeywordEdit::Insert(word, idx) => KeywordEdit::Delete(word.clone(), *idx),
            KeywordEdit::Delete(word, idx) => KeywordEdit::Insert(word.clone(), *idx),
            KeywordEdit::Swap(idx1, idx2) => KeywordEdit::Swap(*idx2, *idx1),
        }
    }
}

/// Represents an edit made to a field in a card.
/// For edit variants that form pairs, the first element in the pair represents
/// the new value, while the second represents the old value. The old value is
/// only necessary for constructing an "undo" of an edit.
#[derive(PartialEq, Eq, Clone, Debug)]
pub enum Edit {
    Title(Title, Title),
    Arcana(Arcana, Arcana),
    Suit(Suit, Suit),
    Number(usize, usize),
    Keyword(KeywordEdit),
}

impl Apply for Edit {
    fn apply(&self, card: &mut Card) {
        match self {
            Edit::Title(new, _) => {
                card.title = new.to_string();
            }
            Edit::Arcana(new, _) => {
                card.arcana = *new;
            }
            Edit::Suit(new, _) => {
                card.suit = *new;
            }
            Edit::Number(new, _) => {
                card.number = *new;
            }
            Edit::Keyword(kw_edit) => {
                kw_edit.apply(card);
            }
        }
    }
}

impl Undo for Edit {
    fn undo(&self) -> Self {
        match self {
            Edit::Title(new, old) => Edit::Title(old.clone(), new.clone()),
            Edit::Arcana(new, old) => Edit::Arcana(old.clone(), new.clone()),
            Edit::Suit(new, old) => Edit::Suit(old.clone(), new.clone()),
            Edit::Number(new, old) => Edit::Number(*old, *new),
            Edit::Keyword(kw_edit) => Edit::Keyword(kw_edit.undo()),
        }
    }
}

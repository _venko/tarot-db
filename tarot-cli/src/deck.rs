use crate::card::Card;
use crate::edit::Edit;

/// Holds all information about a card for a program session.
struct CardRecord {
    /// The card this record pertains to.
    card: Card,
    /// All the edits made to this card during this session.
    edits: Vec<Edit>,
    /// The file path this card was loaded from and will be written to.
    file: String,
}

impl CardRecord {
    pub fn new(card: Card, file: String) -> CardRecord {
        CardRecord {
            card: card,
            edits: Vec::new(),
            file: file,
        }
    }
}

/// Represents a deck of cards.
pub struct Deck(Vec<CardRecord>);

impl Deck {
    pub fn add(&mut self, card: Card, file: String) {
        let new_rec = CardRecord::new(card, file);
        let Deck(ref mut cards) = self;
        cards.push(new_rec);
    }
}
use serde_yaml::Error;

/// Represents an arcana of tarot cards.
#[derive(PartialOrd, Ord, PartialEq, Eq, Debug, Copy, Clone, Serialize, Deserialize)]
pub enum Arcana {
    Major,
    Minor,
}

/// Represents a suit of tarot cards.
#[derive(PartialOrd, Ord, PartialEq, Eq, Debug, Copy, Clone, Serialize, Deserialize)]
pub enum Suit {
    Major,
    Wands,
    Cups,
    Swords,
    Pentacles,
}

/// Represents a tarot card.
#[derive(PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct Card {
    /// The full name of the card.
    pub title: String,
    /// The arcana of the card (major or minor).
    pub arcana: Arcana,
    /// The suit of the card (cups, wands, etc.).
    pub suit: Suit,
    /// The number of the card.
    pub number: usize,
    /// Keywords associated with the card.
    pub keywords: Vec<String>,
}

impl PartialOrd for Card {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let suit_ord = self.suit.cmp(&other.suit);
        match suit_ord {
            std::cmp::Ordering::Equal => self.number.partial_cmp(&other.number),
            _ => Some(suit_ord),
        }
    }
}

impl Ord for Card {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let suit_ord = self.suit.cmp(&other.suit);
        match suit_ord {
            std::cmp::Ordering::Equal => self.number.cmp(&other.number),
            _ => suit_ord,
        }
    }
}

impl Card {
    /// Deserializes a Card from a YAML string.
    pub fn from_yaml(yaml: String) -> Result<Card, Error> {
        return serde_yaml::from_str(&yaml);
    }

    /// Serializes a Card to a YAML string.
    pub fn to_yaml(&self) -> Result<String, Error> {
        return serde_yaml::to_string(self);
    }
}
